# mrfioc2 conda recipe

Home: "https://github.com/icshwi/mrfioc2"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS mrfioc2 module
